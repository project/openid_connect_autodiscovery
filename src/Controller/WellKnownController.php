<?php

namespace Drupal\openid_connect_autodiscovery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\oauth2_server\OAuth2StorageInterface;
use Drupal\oauth2_server\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Wellknown controller class.
 */
class WellKnownController extends ControllerBase {

  /**
   * The OAuth2Storage.
   *
   * @var \Drupal\oauth2_server\OAuth2StorageInterface
   */
  protected $storage;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The constructor.
   *
   * @param \Drupal\oauth2_server\OAuth2StorageInterface $oauth2_storage
   *   The OAuth2 storage object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory object.
   */
  public function __construct(
      OAuth2StorageInterface $oauth2_storage,
      LoggerChannelFactoryInterface $loggerFactory
  ) {
    $this->storage = $oauth2_storage;
    $this->loggerFactory = $loggerFactory;
    $this->logger = $this->loggerFactory->get('openid_connect_autodiscovery');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('oauth2_server.storage'),
      $container->get('logger.factory')
    );
  }

  /**
   * Certificates.
   *
   * Clone from oauth2_service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route found.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request in use.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response object.
   */
  public function certificates(RouteMatchInterface $route_match, Request $request) {
    $keys = Utility::getKeys();
    $certificates = [];
    $certificates[] = $keys['public_key'];
    return new JsonResponse($certificates);
  }

  /**
   * Return the JWKS endpoint JSON.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response object.
   */
  public function jwksEndpoint(RouteMatchInterface $route_match, Request $request) {
    $keys = Utility::getKeys();
    $cert = openssl_x509_read($keys['public_key']);
    $publicKey = openssl_get_publickey($cert);
    openssl_x509_free($cert);
    $keyDetails = openssl_pkey_get_details($publicKey);
    openssl_pkey_free($publicKey);
    $jsonKeys['e'] = base64_encode($keyDetails['rsa']['e']);
    $jsonKeys['n'] = base64_encode($keyDetails['rsa']['n']);
    $jsonKeys['mod'] = self::base64urlEncode($keyDetails['rsa']['n']);
    $jsonKeys['exp'] = self::base64urlEncode($keyDetails['rsa']['e']);
    $jsonKeys['x5c'][] = base64_encode(self::pem2der($keys['public_key']));
    $jsonKeys['kty'] = 'RSA';
    $jsonKeys['use'] = "sig";
    $jsonKeys['alg'] = "RS256";

    $json = [
      "keys" => [
        $jsonKeys,
      ],
    ];
    $opensslError = openssl_error_string();

    if ($opensslError !== FALSE) {
      $this->logger->error("JWKS-json Generator OpenSSL Error [@message]", [
        "@message" => $opensslError,
      ]);
      throw new HttpException(522, "SSL subsytem failure detected");
    }
    return new JsonResponse($json, 200);
  }

  /**
   * Return the autodiscovery JSON.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response object.
   */
  public function openidDiscovery(RouteMatchInterface $route_match, Request $request) {
    $base_root = $request->getSchemeAndHttpHost();
    $base_url = $base_root;
    $oidc = [];
    $oidc['issuer'] = $base_root . "/";
    $oidc["authorization_endpoint"] = $base_url . "/oauth2/authorize";
    $oidc["token_endpoint"] = $base_url . "/oauth2/token";
    $oidc["userinfo_endpoint"] = $base_url . "/oauth2/UserInfo";
    $oidc["end_session_endpoint"] = $base_url . "/user/logout";
    $oidc["subject_types_supported"] = ["public"];
    $oidc["response_types_supported"] = ["code", "token"];
    $oidc["jwks_uri"] = $base_url . "/.well-known/openid-jwks";
    $oidc['id_token_signing_alg_values_supported'] = "RS256";
    return new JsonResponse($oidc);
  }

  /**
   * Convert a PEM encoded to DER encoded certificate.
   *
   * @param string $pem
   *   The PEM encoded certificate.
   *
   * @return string|bool
   *   The DER encoded certificate or false.
   *
   * @see http://php.net/manual/en/ref.openssl.php#74188
   */
  public static function pem2der($pem) {
    $begin = "CERTIFICATE-----";
    $end = "-----END";
    $pem = substr($pem, strpos($pem, $begin) + strlen($begin));
    $pem = substr($pem, 0, strpos($pem, $end));
    $der = base64_decode($pem);
    return $der;
  }

  /**
   * Base64 URL encoding as used by OIDC.
   *
   * @param string $data
   *   The data to encode.
   *
   * @return string
   *   The base64 url encoded string.
   *
   * @see http://php.net/manual/en/function.base64-encode.php#103849
   */
  public static function base64urlEncode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }

}
