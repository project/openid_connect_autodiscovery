This module exposes the `/.well-known/openid-configuration`
endpoint as defined for use with OpenId Connect Autodiscovery
It is based on the Specification as listed on [openid.net/specs/openid-connect-discovery-1_0](https://openid.net/specs/openid-connect-discovery-1_0.html)

It needs the Drupal oauth2 server got the actual OIDC (OpenId Connect)

Error Code implemented:
 - 522 SSL subsytem failure detected.

Endpoints implemented:
 - __authorization_endpoint__: "/oauth2/authorize"
 - __token_endpoint__: "/oauth2/token"
 - __userinfo_endpoint__: "/oauth2/UserInfo"
 - __end_session_endpoint__: "/user/logout"
 - __jwks_uri__: "/.well-known/openid-jwks"
